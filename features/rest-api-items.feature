Feature: Rest api for items
    CRUD API for items
    Scenario: List items
        Given I am a user
        When I send GET request to /items
        Then I should get 200 ok response

    Scenario: Create items
        Given I am a user
        When I send POST request to /items
        Then I should get 201 created response