module.exports = {
  clearMocks: true,
  testEnvironment: "node",
  testTimeout: 30000,
  transform: {
      '^.+\\.tsx?$': 'ts-jest'
  },
  preset: '@shelf/jest-mongodb',

};