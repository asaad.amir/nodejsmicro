import { Item } from "../src/models/item";

import { connect_to_mongo } from "./utils";

describe("Item model", () => {
  beforeAll(async () => {
    await connect_to_mongo();
  });

  it("Item.build() should create new record", async (done) => {
    const item = await Item.build({
      title: "item1",
      description: "desc",
    }).save();
    const itemDB = await Item.findById(item._id);
    expect(itemDB.toJSON().title).toEqual(item.toJSON().title);
    done();
  });
});
