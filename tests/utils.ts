import mongoose from "mongoose";

export function connect_to_mongo() {
  mongoose.connect(
    process.env.MONGO_URL,
    { useNewUrlParser: true, useCreateIndex: true },
    (err) => {
      if (err) {
        console.error(err);
        process.exit(1);
      }
    }
  );
}
