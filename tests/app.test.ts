import { getApp } from "../src/app";
import request from "supertest";
import { Item } from "../src/models/item";
import { connect_to_mongo } from "./utils";

const app = getApp();

describe("POST /items", () => {
  beforeAll(async () => {
    await connect_to_mongo();
  });

  it("should create item", (done) => {
    request(app)
      .post("/items")
      .send({ title: "title", description: "description" })
      .set("Accept", "application/json")
      .expect(201)
      .expect("Content-Type", /json/)
      .then((response) => {
        expect(response.body.title).toBe("title");
        expect(response.body.description).toBe("description");
        expect(response.body).toHaveProperty("_id");
        done();
      })
      .catch((err) => done(err));
  });
  it("Should return 400 for invalid params", (done) => {
    request(app)
      .post('/items')
      .send('')
      .expect(400);
    done()
  });
});

describe("GET /items", () => {
  beforeEach(async () => {
    await Item.build({ title: "item1", description: "desc" }).save();
    await Item.build({ title: "item2", description: "desc" }).save();
  });

  it("should return list of items", async (done) => {
    request(app)
      .get("/items")
      .set("Accept", "application/json")
      .expect(200)
      .expect("Content-Type", "application/json; charset=utf-8")
      .then((response) => {
        expect(response.body.result).toBeInstanceOf(Array);
        done();
      })
      .catch((err) => done(err));
  });
});

describe("GET /items/:id", () => {
  let item;
  beforeEach(async () => {
    item = await Item.build({ title: "item1", description: "desc" }).save();
  });
  it("should return item by id", async (done) => {
    request(app)
      .get(`/items/${item._id}/`)
      .expect(200)
      .then((response) => {
        expect(response.body.title).toBe(item.toJSON().title);
        done();
      })
      .catch((err) => done(err));
  });
});

describe("PUT /items/:id", () => {
  let item;
  beforeEach(async () => {
    item = await Item.build({ title: "item1", description: "desc" }).save();
  });
  it("should update item by id", async (done) => {
    request(app)
      .put(`/items/${item._id}`)
      .send({ title: "new title", description: "new description" })
      .expect(200)
      .then((response) => {
        expect(response.body.title).toBe("new title");
        expect(response.body.description).toBe("new description");
        done();
      })
      .catch((err) => done(err));
  });
});

describe("Delete /items/:id", () => {
  let item;
  beforeEach(async () => {
    item = await Item.build({ title: "item1", description: "desc" }).save();
  });
  it("should delete item by id", async (done) => {
    const item_id = item._id;
    request(app)
      .delete(`/items/${item_id}/`)
      .expect(200)
      .then((response) => {
        expect(response.body.result).toMatchObject({
          n: 1,
          ok: 1,
          deletedCount: 1,
        });
        expect(JSON.stringify(response.body.deleted)).toEqual(
          JSON.stringify(item_id)
        );
        done();
      })
      .catch((err) => done(err));
  });
});
