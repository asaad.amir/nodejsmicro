# Microservice for CRUD operations


## Getting started

```bash
docker-compose up -d --build
```
And then visit http://localhost:3000/docs/

for logs:
```bash
docker-compose server logs
```

## Build & Testing

```bash
# Build
npm run build

# Test
npm test
```