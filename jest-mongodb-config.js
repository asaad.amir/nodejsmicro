module.exports = {
  mongodbMemoryServerOptions: {
    binary: {
      version: '4.0.3',
      skipMD5: true,
      // systemBinary?: process.env.MONGOMS_SYSTEM_BINARY
    },
    instance: {
      dbName: 'jest'
    },
    autoStart: false
  }
};
