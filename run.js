const { app } = require('deta');

// define a function to run from the cli
// the function must take an event as an argument
app.lib.run(event => "Welcome to Deta!");

module.exports = app;