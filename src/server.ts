import { getApp } from "./app";
import http from "http";
import mongoose from "mongoose";

import dotenv from "dotenv";

dotenv.config();

const port = process.env.PORT; //3000
const host = process.env.HOST; // 'localhost'
const mongoHost = process.env.MONGO_HOST; //'mongo'
const mongoPort = process.env.MONGO_PORT; //27017
const mongoDB = process.env.MONGO_DB; //'node-app'

const app = getApp();

mongoose.connect(
  `mongodb://${mongoHost}:${mongoPort}/${mongoDB}`,
  {
    useCreateIndex: true,
  },
  () => {
    console.log("connected to MongoDB");
    http.createServer({}, app).listen(port, () => {
      console.log(`Listening on http://${host}:${port}..`);
    });
  }
);
