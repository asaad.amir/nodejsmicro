import * as mongoose from "mongoose";

interface IItem {
  title: string;
  description: string;
}

interface ItemModelInterface extends mongoose.Model<any> {
  build(attrs: IItem): ItemDoc;
}

interface ItemDoc extends mongoose.Document {
  title: string;
  description: string;
}

const itemSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
});

itemSchema.statics.build = (attr: IItem) => {
  return new Item(attr);
};

const Item = mongoose.model<ItemDoc, ItemModelInterface>("Item", itemSchema);

export { Item };
