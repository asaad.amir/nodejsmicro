const apiDoc = {
  swagger: '2.0',
  basePath: '/v1',
  info: {
    title: 'Items REST API doc',
    version: '1.0.0'
  },
  definitions: {
    World: {
      type: 'object',
      properties: {
        name: {
          description: 'REST API Items documentation',
          type: 'string'
        }
      },
      required: ['name']
    }
  },
  paths: {}
};

export default apiDoc;
