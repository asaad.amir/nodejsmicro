import express from "express";
import { Express } from "express";
import { Request, Response } from "express";
import expressOasGenerator from "express-oas-generator";
import mongoose from "mongoose";

import { Item } from "./models/item";

export function getApp(): Express {
  const app = express();
  expressOasGenerator.init(
    app,
    function (spec) {
      return spec;
    },
    "",
    60 * 100,
    "docs",
    mongoose.modelNames()
  );
  app.use(
    express.json({
      inflate: true,
      limit: "100kb",
      reviver: null,
      strict: true,
      type: "application/json",
      verify: undefined,
    })
  );

  app.post("/items", async (req: Request, res: Response, next) => {
    const { title, description } = req.body;
    const item = Item.build({ title, description });
    await item.save((err, item) => {
      if (err) {
        res.status(400).send({ 'error': 'Bad Request' })
        next(err)
      } else {
        res.status(201).json(item.toJSON());
        next()
      }
    })

  });

  app.get("/items", async (_req: Request, res: Response) => {
    const items = await Item.find({}, "title description").lean();
    res.send({
      result: items,
    });
  });

  app.get("/items/:id", async (req: Request, res: Response) => {
    const item = await Item.findById(req.params.id).exec();
    res.json(item.toJSON());
  });

  app.put("/items/:id", async (req: Request, res: Response) => {
    await Item.updateOne({ _id: req.params.id }, req.body);
    const item = await Item.findById(req.params.id).exec();
    res.json(item);
  });

  app.delete("/items/:id", async (req: Request, res: Response) => {
    const result = await Item.deleteOne({ _id: req.params.id }).exec();
    res.send({ result: result, deleted: req.params.id });
  });

  return app;
}
